﻿using Emgu.CV;
using Emgu.CV.Structure;

namespace Poser.Model
{
    /// <summary>
    /// Defines the results of the calibration process
    /// </summary>
    public class CalibrationResult
    {
        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public CalibrationResult()
        {
            // Extra Initialization can go here
        }

        /// <summary>
        /// Clone constructor
        /// </summary>
        /// <param name="result">The result that we are cloning</param>
        public CalibrationResult(CalibrationResult result)
        {
            Focal = result.Focal;
            Sx = result.Sx;
            Centre = result.Centre;
            Rotation = result.Rotation.Clone();
            Translation = result.Translation.Clone();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The focal length
        /// </summary>
        public double Focal { get; set; }

        /// <summary>
        /// The scaling factor in the x-direction
        /// </summary>
        public double Sx { get; set; }

        /// <summary>
        /// Optic Centre
        /// </summary>
        public MCvPoint2D64f Centre { get; set; }

        /// <summary>
        /// Extrinsic rotation of the camera
        /// </summary>
        public Matrix<double> Rotation { get; set; }

        /// <summary>
        /// Extrinsic translation of the camera
        /// </summary>
        public Matrix<double> Translation { get; set; }

        #endregion
    }
}
