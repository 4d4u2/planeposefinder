﻿namespace Poser.Global
{
    /// <summary>
    /// Defines the main path for the application
    /// </summary>
    public class Paths
    {
        #region Path Constants

        public static readonly string CONFIG_FILE = "Input/Config.xml";
        public static readonly string INPUT_FOLDER = "Input/";
 
        #endregion
    }
}