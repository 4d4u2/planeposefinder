﻿using Emgu.CV;
using Emgu.CV.Structure;
using Poser.Model;
using System.Collections.Generic;
using System.Linq;

namespace Poser.Tsai
{
    /// <summary>
    /// Defines a focal finder class for finding F and Tz
    /// </summary>
    public class FocalFinder
    {
        #region Private Variables

        private Matrix<double> _a;
        private Matrix<double> _b;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="featurePoints">The feature points that we are working with</param>
        /// <param name="centre">The centre of the image</param>
        /// <param name="rotation">The current rotation matrix</param>
        /// <param name="ty">The current value associated with ty</param>
        public FocalFinder(List<GridPoint> featurePoints, MCvPoint2D64f centre, Matrix<double> rotation, double ty) 
        {
            _a = InitializeMatrixA(featurePoints, centre, rotation, ty);
            _b = InitializeMatrixB(featurePoints, centre, rotation);
        }

        #endregion

        #region Getter Methods

        /// <summary>
        /// Retrieve the values associated with the A matrix
        /// </summary>
        /// <returns>The A matrix that we are working with</returns>
        public Matrix<double> GetAMatrix() 
        {
            return _a;
        }

        /// <summary>
        /// Retrieve the values associated with the B Matrix
        /// </summary>
        /// <returns>The B matrix that we are working with</returns>
        public Matrix<double> GetBMatrix() 
        {
            return _b;
        }

        #endregion

        #region Find the Solution

        /// <summary>
        /// Calculate the result of processing
        /// </summary>
        /// <returns>The result of processing the associated points</returns>
        public Matrix<double> CalculateResult() 
        {
            var result = new Matrix<double>(2, 1);
            CvInvoke.Solve(_a, _b, result, Emgu.CV.CvEnum.DecompMethod.Svd);
            return result;
        }

        #endregion

        #region Initializer Helpers

        /// <summary>
        /// Defines a set of initializer helpers
        /// </summary>
        /// <param name="features">The list of feature points that we are dealing with</param>
        /// <param name="centre">The centre of the image</param>
        /// <param name="rotation">The rotation that we are using</param>
        /// <param name="ty">The current translation in the y direction</param>
        /// <returns>The resultant A matrix</returns>
        private Matrix<double> InitializeMatrixA(List<GridPoint> features, MCvPoint2D64f centre, Matrix<double> rotation, double ty)
        {
            var matrix = new Matrix<double>(features.Count, 2); int counter = 0;
            for (int i = 0; i < features.Count; i++) 
            {
                matrix[counter, 0] =  features[i].ScenePoint.X * rotation[1, 0];
                matrix[counter, 0] += features[i].ScenePoint.Y * rotation[1, 1];
                matrix[counter, 0] += features[i].ScenePoint.Z * rotation[1, 2] + ty;
                matrix[counter, 1] = -(features[i].ImagePoint.Y - centre.Y);
                counter++;
            }
            return matrix;        
        }

        /// <summary>
        /// Defines a set of initializer helpers
        /// </summary>
        /// <param name="features">The list of feature points that we are dealing with</param>
        /// <param name="centre">The centre of the image</param>
        /// <param name="rotation">The rotation that we are using</param>
        /// <returns>The resultant B matrix</returns>
        private Matrix<double> InitializeMatrixB(List<GridPoint> features, MCvPoint2D64f centre, Matrix<double> rotation)
        {
            var matrix = new Matrix<double>(features.Count, 1); int counter = 0;
            for (int i = 0; i < features.Count; i++)
            {
                matrix[counter, 0] =  features[i].ScenePoint.X * rotation[2, 0];
                matrix[counter, 0] += features[i].ScenePoint.Y * rotation[2, 1]; 
                matrix[counter, 0] += features[i].ScenePoint.Z * rotation[2, 2];
                matrix[counter, 0] *= (features[i].ImagePoint.Y - centre.Y);
                counter++;
            }
            return matrix;
        }

        #endregion
    }
}