﻿using Emgu.CV;
using Emgu.CV.Structure;
using Pose.Utility;
using Poser.Model;
using Poser.Utility;
using System;
using System.Collections.Generic;
using System.IO;

namespace Poser.Controller
{
    /// <summary>
    /// Main controller associated with the application
    /// </summary>
    public class MainController
    {
        #region Private Variables

        private Arguments _arguments;
        private FramePoints _points1;
        private FramePoints _points2;

        private Distortion _distortion1;
        private Distortion _distortion2;

        #endregion

        #region Constructors 

        /// <summary>
        /// Main Constructor
        /// </summary>
        public MainController() 
        {
            _arguments = new Arguments();

            _points1 = PointLoader.Load(_arguments.PointSetPath1, _arguments.Parameters);
            _points2 = PointLoader.Load(_arguments.PointSetPath2, _arguments.Parameters);

            _distortion1 = DistortionLoader.Load(_arguments.DistortionPath1, _arguments.Parameters);
            _distortion2 = DistortionLoader.Load(_arguments.DistortionPath2, _arguments.Parameters);
        }

        #endregion

        #region Execution Logic

        /// <summary>
        /// Main Execution Logic
        /// </summary>
        public void Run() 
        {
            Console.WriteLine("Finding Relative Poses....");
            var rotation1 = RotationFinder.Find(_arguments.Parameters, _distortion1, _points1);
            var rotation2 = RotationFinder.Find(_arguments.Parameters, _distortion2, _points2);

            var angle1 = FindAngle(rotation1); var angle2 = FindAngle(rotation2);

            Console.WriteLine("Rotation Angle: {0}", angle1); Console.WriteLine("Rotation Angle: {0}", angle2);

            Console.WriteLine("Refine the first pose....");
            var center1 = new MCvPoint2D64f(_distortion1.CX, _distortion1.CY);
            var pose1 = PoseRefiner.Refine(_arguments.Parameters, center1, rotation1, _points1);

            Console.WriteLine("Refine the second pose...");
            var center2 = new MCvPoint2D64f(_distortion2.CX, _distortion2.CY);
            var pose2 = PoseRefiner.Refine(_arguments.Parameters, center2, rotation2, _points2);

            Console.WriteLine("Writing the first results to disk....");
            WriteResult(_arguments.OutputPath1, pose1, _points1);

            Console.WriteLine("Writing the second results to disk....");
            WriteResult(_arguments.OutputPath2, pose2, _points2);
        }

        #endregion

        #region Angle Discovery Calculations

        /// <summary>
        /// Find the angle between the two given poses
        /// </summary>
        /// <param name="rotationMatrix">The rotation matrix that we are finding the rotation fro</param>
        /// <returns>The result of the disovery</returns>
        private double FindAngle(Matrix<double> rotationMatrix)
        {
            var rotationVector = new Matrix<double>(3, 1); CvInvoke.Rodrigues(rotationMatrix, rotationVector);
            var angle = CvInvoke.Norm(rotationVector);
            return 180 - (angle * 180 / Math.PI);
        }

        #endregion

        #region Writer Logic

        /// <summary>
        /// Write the result to disk
        /// </summary>
        /// <param name="path">The path that we are writing to</param>
        /// <param name="pose">The pose of the right plane</param>
        /// <param name="points">The points that we are writing to disk</param>
        private void WriteResult(string path, Matrix<double> pose, FramePoints points) 
        {
            var tgrid2 = Scorer.TransformPoints(pose, points.RightPoints);
            var newPoints = new List<GridPoint>(points.LeftPoints); newPoints.AddRange(tgrid2);
            WritePoints(path, newPoints);

            var fileName = Path.GetFileNameWithoutExtension(path);
            var fullFileName = string.Format("{0}.ply", fileName);
            var plyPath = Path.Combine("Models", fullFileName);
            PlyWriter.Write(plyPath, newPoints);
        }

        /// <summary>
        /// Write the path to disk
        /// </summary>
        /// <param name="path">The path taht we are dealing with</param>
        /// <param name="points">The points that we are writing to disk</param>
        private void WritePoints(string path, List<GridPoint> points)
        {
            using (var writer = new StreamWriter(path)) 
            {
                foreach (var point in points) 
                {
                    var s = point.ScenePoint; var i = point.ImagePoint;
                    var line = string.Format("{0} {1} {2} {3} {4}", s.X, s.Y, s.Z, i.X, i.Y);
                    writer.WriteLine(line);                
                }
            }
        }

        #endregion
    }
}
