﻿using Poser.Controller;
using System;

namespace Poser
{
    /// <summary>
    /// Main application entry point
    /// </summary>
    public class Program
    {
        #region Main Method
        
        /// <summary>
        /// Main entry point for the application
        /// </summary>
        public static void Main()
        {
            try
            {
                new MainController().Run(); Pause();
            }
            catch (Exception exception) 
            {
                Console.Error.WriteLine("Error: {0}", exception.Message); Pause();
            }
        }

        #endregion

        #region Utilitiies

        /// <summary>
        /// Pause the screen
        /// </summary>
        private static void Pause()
        {
            Console.WriteLine(); Console.WriteLine("Press ENTER to continue...");
            Console.ReadLine();
        }

        #endregion
    }
}
