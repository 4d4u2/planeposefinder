﻿using Emgu.CV;
using Emgu.CV.Structure;
using Poser.Model;
using System;
using System.Collections.Generic;

namespace Poser.Utility
{
    /// <summary>
    /// Defines the logic to define a set of repo errors for a given set of points
    /// </summary>
    public class ErrorFinder
    {
        #region Functionality Entry Point

        /// <summary>
        /// Determine the associated errors
        /// </summary>
        /// <param name="points">The points that we are dealing with</param>
        /// <param name="calibration">The calibration</param>
        /// <param name="errors">The errors</param>
        /// <returns>A total error</returns>
        public static double GetError(List<GridPoint> points, CalibrationResult calibration, double[] errors) 
        {
            var total = 0.0;

            var cameraMatrix = BuildCameraMatrix(calibration);
            var pose = BuildPose(calibration);

            int counter = 0;
            foreach (var feature in points) 
            {
                var tPoint = Transform(pose, feature.ScenePoint);
                var pPoint = Project(cameraMatrix, tPoint);
                var residual = FindResidual(feature.ImagePoint, pPoint);
                total += residual; errors[counter++] = residual; 
            }

            return total;
        }

        #endregion

        #region Matrix Constructors

        /// <summary>
        /// Build the associated camera matrix
        /// </summary>
        /// <param name="problem">The problem that we are trying to solve</param>
        /// <returns>The problem that we are solving</returns>
        private static Matrix<double> BuildCameraMatrix(CalibrationResult problem)
        {
            var result = new Matrix<double>(3, 3); result.SetIdentity();

            result.Data[0, 0] = problem.Focal * problem.Sx;
            result.Data[0, 2] = problem.Centre.X;
            result.Data[1, 1] = problem.Focal;
            result.Data[1, 2] = problem.Centre.Y;

            return result;
        }

        /// <summary>
        /// Build the pose matrix from the problem
        /// </summary>
        /// <param name="problem">The problem that we are extracting the pose from</param>
        /// <returns>The matrix holding the pose</returns>
        private static Matrix<double> BuildPose(CalibrationResult problem)
        {
            var result = new Matrix<double>(4, 4); result.SetIdentity();

            for (int row = 0; row < 3; row++) 
            {
                for (int column = 0; column < 3; column++) 
                {
                    result.Data[row, column] = problem.Rotation[row, column];
                }
            }

            result.Data[0, 3] = problem.Translation.Data[0, 0];
            result.Data[1, 3] = problem.Translation.Data[1, 0];
            result.Data[2, 3] = problem.Translation.Data[2, 0];

            return result;
        }

        #endregion

        #region Image Transformation Helpers

        /// <summary>
        /// Defines the logic to transform the given point associated by the pose
        /// </summary>
        /// <param name="pose">The pose that we are transforming</param>
        /// <param name="point">The point that is being transformed</param>
        /// <returns>The resultant point</returns>
        private static MCvPoint3D64f Transform(Matrix<double> pose, Emgu.CV.Structure.MCvPoint3D64f point)
        {
            var X = point.X * pose.Data[0, 0] + point.Y * pose.Data[0, 1] + point.Z * pose.Data[0, 2] + pose.Data[0, 3];
            var Y = point.X * pose.Data[1, 0] + point.Y * pose.Data[1, 1] + point.Z * pose.Data[1, 2] + pose.Data[1, 3];
            var Z = point.X * pose.Data[2, 0] + point.Y * pose.Data[2, 1] + point.Z * pose.Data[2, 2] + pose.Data[2, 3];
            return new MCvPoint3D64f(X, Y, Z);
        }

        /// <summary>
        /// Project a 3-D point into 2-D spac
        /// </summary>
        /// <param name="cameraMatrix">The camera matrix that we are using for the projection</param>
        /// <param name="point">The point that we are projecting</param>
        /// <returns>The point that we have projected</returns>
        private static MCvPoint2D64f Project(Matrix<double> cameraMatrix, MCvPoint3D64f point)
        {
            var u = (cameraMatrix.Data[0, 0] * point.X / point.Z) + cameraMatrix.Data[0, 2];
            var v = (cameraMatrix.Data[1, 1] * point.Y / point.Z) + cameraMatrix.Data[1, 2];
            return new MCvPoint2D64f(u, v);
        }

        /// <summary>
        /// Calculate the difference between the given set of two points
        /// </summary>
        /// <param name="point1">The first point that we are getting values for</param>
        /// <param name="point2">The second point that we are getting values for</param>
        /// <returns>The residual between points that we are getting values for</returns>
        private static double FindResidual(MCvPoint2D64f point1, MCvPoint2D64f point2)
        {
            var xDiff = point1.X - point2.X;
            var yDiff = point1.Y - point2.Y;
            return Math.Sqrt(xDiff * xDiff + yDiff * yDiff);
        }

        #endregion
    }
}