﻿using Emgu.CV;
using Emgu.CV.Structure;
using Poser.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace Poser.Utility
{
    /// <summary>
    /// Attempts to find the rotation of the given image points
    /// </summary>
    public class PlaneOrientation
    {
        #region Private Variables

        private Parameters _parameters;
        private List<MCvPoint3D64f> _scenePoints;
        private List<MCvPoint2D64f> _imagePoints;
        private MCvPoint2D64f _center;

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="parameters">The parameters associated with the applciation</param>
        /// <param name="center">The optic center</param>
        /// <param name="points">The points that we are finding the rotation for</param>
        public PlaneOrientation(Parameters parameters, MCvPoint2D64f center, List<GridPoint> points) 
        {
            _parameters = parameters; _center = center;
            _imagePoints = points.ConvertAll(point => point.ImagePoint);
            _scenePoints = points.ConvertAll(point => point.ScenePoint);
        }

        #endregion

        #region Refinement Entry Point

        /// <summary>
        /// Find the rotation matrix associated with the given points
        /// </summary>
        /// <returns>A 3x3 rotation matrix</returns>
        public Matrix<double> Find() 
        {
            // Assemble the parameters
            var imagePoints = _imagePoints.ConvertAll(point => new PointF((float) point.X, (float) point.Y)).ToArray();
            var scenePoints = _scenePoints.ConvertAll(point => new MCvPoint3D32f((float) point.X, (float) point.Y, (float) point.Z)).ToArray();
            var cameraMatrix = BuildCameraMatrix(_parameters, _center);
            var distortionMatrix = new Matrix<float>(4,1); distortionMatrix.SetZero();

            // Estimate the pose
            Mat rotation = new Mat(); Mat translation = new Mat();
            var result = CvInvoke.SolvePnP(scenePoints, imagePoints, cameraMatrix, distortionMatrix, rotation, translation, false, Emgu.CV.CvEnum.SolvePnpMethod.Iterative);
            var inliers = new Mat();

            var imagePointV = new Emgu.CV.Util.VectorOfPointF(imagePoints);
            var scenePointV = new Emgu.CV.Util.VectorOfPoint3D32F(scenePoints);

            //CvInvoke.SolvePnPRansac(scenePointV, imagePointV, cameraMatrix, distortionMatrix, rotation, translation, false, 100, 8, 0.99, inliers, Emgu.CV.CvEnum.SolvePnpMethod.Iterative);
            var pose = GetPose(rotation, translation);

            // Calcualte the accuracy
            var testPose = new Matrix<double>(4, 4); testPose.SetIdentity();
            var score = FindAccuracy(scenePoints, imagePoints, cameraMatrix, pose);
            Console.WriteLine("Error: {0}", score);
            
            // Return the result
            return pose;
        }

        #endregion

        #region Construct Camera Matrix

        /// <summary>
        /// Construct a camera matrix
        /// </summary>
        /// <param name="parameters">The parameters we are using as a basis for construction</param>
        /// <param name="center">the cener</param>
        /// <returns>The resultant parameters</returns>
        private Matrix<float> BuildCameraMatrix(Parameters parameters, MCvPoint2D64f center)
        {
            var result = new Matrix<float>(3, 3); result.SetIdentity();

            result.Data[0, 0] = (float)Math.Max(parameters.Width, parameters.Height);
            result.Data[0, 2] = (float)center.X;
            result.Data[1, 1] = result.Data[0, 0];
            result.Data[1, 2] = (float)center.Y;

            return result; 
        }

        #endregion

        #region Pose Finder

        /// <summary>
        /// Constructor a pose matrix
        /// </summary>
        /// <param name="rotation">The associated rotation matrix</param>
        /// <param name="translation">The associated translation matrix</param>
        /// <returns>The resultant pose</returns>
        private Matrix<double> GetPose(Mat rotation, Mat translation)
        {
            // Setup the result matrix
            var result = new Matrix<double>(4, 4); result.SetIdentity();
            
            // Extract the data into an image structure
            var rotationImage = rotation.ToImage<Gray, float>();
            var translationImage = translation.ToImage<Gray, float>();

            // Copy across the rotation data
            var rotationVector = new Matrix<double>(new double[] { rotationImage.Data[0, 0, 0], rotationImage.Data[1, 0, 0], rotationImage.Data[2, 0, 0] });
            var rotationMatrix = new Matrix<double>(3, 3); CvInvoke.Rodrigues(rotationVector, rotationMatrix);
            for (int row = 0; row < 3; row++) 
            {
                for (int column = 0; column < 3; column++) 
                {
                    result.Data[row, column] = rotationMatrix.Data[row, column];
                }            
            }

            // Copy across the translation            
            result.Data[0, 3] = translationImage.Data[0, 0, 0];
            result.Data[1, 3] = translationImage.Data[1, 0, 0];
            result.Data[2, 3] = translationImage.Data[2, 0, 0];

            // Return the result
            return result;
        }

        #endregion

        #region Accuracy Finder

        /// <summary>
        /// Find the accuracy of the given pose
        /// </summary>
        /// <param name="scenePoints">The scene points</param>
        /// <param name="imagePoints">The image points</param>
        /// <param name="cameraMatrix">The camera matrix</param>
        /// <param name="pose">The associated pose</param>
        /// <returns>The resultant pose</returns>
        private double FindAccuracy(MCvPoint3D32f[] scenePoints, PointF[] imagePoints, Matrix<float> cameraMatrix, Matrix<double> pose)
        {
            var tPoints = Transform(pose, scenePoints.ToList());
            var projected = Project(cameraMatrix, tPoints);
            var difference = FindDifferences(projected, imagePoints.ToList());

            //PointDisplayer.Show("Points", imagePoints.ToList(), projected); CvInvoke.WaitKey();

            return difference;
        }

        /// <summary>
        /// Defines the logic to transform points
        /// </summary>
        /// <param name="pose">The pose that we are transforming to</param>
        /// <param name="points">The points that we are transforming</param>
        /// <returns>The resultant list of points</returns>
        private List<MCvPoint3D32f> Transform(Matrix<double> pose, List<MCvPoint3D32f> points)
        {
            var result = new List<MCvPoint3D32f>();

            foreach (var point in points) 
            {
                var X = pose.Data[0, 0] * point.X + pose.Data[0, 1] * point.Y + pose.Data[0, 2] * point.Z + pose.Data[0, 3];
                var Y = pose.Data[1, 0] * point.X + pose.Data[1, 1] * point.Y + pose.Data[1, 2] * point.Z + pose.Data[1, 3];
                var Z = pose.Data[2, 0] * point.X + pose.Data[2, 1] * point.Y + pose.Data[2, 2] * point.Z + pose.Data[2, 3];
                result.Add(new MCvPoint3D32f((float)X, (float)Y, (float)Z));
            }

            return result;
        }

        /// <summary>
        /// Projection of the associated point set
        /// </summary>
        /// <param name="cameraMatrix">The camera matrix that we are working with</param>
        /// <param name="points">The points that we are transforming</param>
        /// <returns>The resultant list of transformed points</returns>
        private List<PointF> Project(Matrix<float> cameraMatrix, List<MCvPoint3D32f> points)
        {
            var result = new List<PointF>();

            foreach (var point in points)
            {
                var X = (cameraMatrix.Data[0, 0] * point.X / point.Z) + cameraMatrix.Data[0, 2];
                var Y = (cameraMatrix.Data[1, 1] * point.Y / point.Z) + cameraMatrix.Data[1, 2];
                result.Add(new PointF(X, Y));
            }

            return result;        
        }

        /// <summary>
        /// The list of differences associated with the application
        /// </summary>
        /// <param name="points1">The first set of points</param>
        /// <param name="points2">The second set of points</param>
        /// <returns></returns>
        private double FindDifferences(List<PointF> points1, List<PointF> points2)
        {
            var result = 0.0;

            for (int i = 0; i < points1.Count; i++)
            {
                var diffX = points1[i].X - points2[i].X;
                var diffY = points1[i].Y - points2[i].Y;
                var difference = Math.Sqrt(diffX * diffX + diffY * diffY);
                result += difference;
            }

            return result / points1.Count;
        }

        #endregion
    }
}
