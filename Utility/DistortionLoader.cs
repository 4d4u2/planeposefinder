﻿using Emgu.CV;
using Poser.Model;

namespace Poser.Utility
{
    /// <summary>
    /// Defines the functionality to load distortion from disk
    /// </summary>
    public class DistortionLoader
    {
        #region Load Functionality

        /// <summary>
        /// Load the associated distortion from disk
        /// </summary>
        /// <param name="path">The path of the distortion file</param>
        /// <param name="parameters">The parameters associated with the application</param>
        /// <returns>The resultant distortion that we have loaded</returns>
        public static Distortion Load(string path, Parameters parameters) 
        {
            using (var reader = new FileStorage(path, FileStorage.Mode.FormatXml | FileStorage.Mode.Read)) 
            {
                var distortion = new Distortion(parameters);
                distortion.K1 = reader.GetNode("K1").ReadDouble();
                distortion.K2 = reader.GetNode("K2").ReadDouble();
                distortion.P1 = reader.GetNode("P1").ReadDouble();
                distortion.P2 = reader.GetNode("P2").ReadDouble();
                distortion.CX = reader.GetNode("CX").ReadDouble();
                distortion.CY = reader.GetNode("CY").ReadDouble();
                return distortion;               
            }
        }

        #endregion
    }
}
