﻿using Emgu.CV;
using Emgu.CV.Structure;
using Poser.Model;

namespace Poser.Utility
{
    /// <summary>
    /// Defines the logic to find the relative rotation between the two given point sets
    /// </summary>
    public class RotationFinder
    {
        #region Entry Point

        /// <summary>
        /// Find the the pose between the two points
        /// </summary>
        /// <param name="parameters">The parameters that we are dealing with</param>
        /// <param name="distortion">The current distortion parmeters</param>
        /// <param name="framePoints">The frame points that we a finding the difference</param>
        /// <returns>The relative pose</returns>
        public static Matrix<double> Find(Parameters parameters, Distortion distortion, FramePoints framePoints) 
        {
            var center = new MCvPoint2D64f(distortion.CX, distortion.CY);

            var rotationFinder1 = new PlaneOrientation(parameters, center, framePoints.LeftPoints);
            var pose1 = rotationFinder1.Find();

            var rotationFinder2 = new PlaneOrientation(parameters, center, framePoints.RightPoints);
            var pose2 = rotationFinder2.Find();

            var invPose = new Matrix<double>(4, 4); CvInvoke.Invert(pose1, invPose, Emgu.CV.CvEnum.DecompMethod.Svd);
            var pose = pose2 * invPose;

            return ExtractRotation(pose);
        }

        #endregion

        #region  Utilities

        /// <summary>
        /// Extract the rotation matrix from a pose
        /// </summary>
        /// <param name="pose">The pose that we are extracting from</param>
        /// <returns>The resultant matrix</returns>
        private static Matrix<double> ExtractRotation(Matrix<double> pose)
        {
            var result = new Matrix<double>(3, 3);

            for (int row = 0; row < 3; row++)
            {
                for (int column = 0; column < 3; column++)
                {
                    result.Data[row, column] = pose.Data[row, column];
                }
            }

            return result;
        }

        #endregion
    }
}
