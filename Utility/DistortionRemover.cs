﻿using Poser.Model;
using System.Collections.Generic;

namespace Poser.Utility
{
    /// <summary>
    /// Defines the logic to remove distortion from points
    /// </summary>
    public class DistortionRemover
    {
        #region Process Points

        /// <summary>
        /// Process the points to remove distortion
        /// </summary>
        /// <param name="parameters">The appliation parameters</param>
        /// <param name="distortion">The distortion that we are dealing with</param>
        /// <param name="framePoints">The frame points that we are dealing with</param>
        /// <returns>The frame points that we are dealing with</returns>
        public static FramePoints Process(Parameters parameters, Distortion distortion, FramePoints framePoints) 
        {
            var distorter = new Distorter();

            var leftPoints = RemoveDistortion(distorter, distortion, framePoints.LeftPoints);
            var rightPoints = RemoveDistortion(distorter, distortion, framePoints.RightPoints);

            return new FramePoints(leftPoints, rightPoints);
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Remove distortion from the points
        /// </summary>
        /// <param name="distorter">The distorter logic</param>
        /// <param name="distortion">The distortion parameters</param>
        /// <param name="points">The points that we are dealing with</param>
        /// <returns>The new list of points that we are dealing with</returns>
        private static List<GridPoint> RemoveDistortion(Distorter distorter, Distortion distortion, List<GridPoint> points)
        {
            var uPoints = new List<GridPoint>();

            foreach (var point in points) 
            {
                var imagePoint = distorter.UnDistort(distortion, point.ImagePoint);
                uPoints.Add(new GridPoint(point.ScenePoint, imagePoint));
            }

            return uPoints;
        }

        #endregion
    }
}
