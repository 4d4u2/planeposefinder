﻿using Emgu.CV;
using Emgu.CV.Structure;
using LMDotNet;
using Poser.Model;
using System;
using System.Linq;

namespace Poser.Utility
{
    /// <summary>
    /// Defines the logic to refine the given pose
    /// </summary>
    public class PoseRefiner
    {
        #region Refinement Entry Point

        /// <summary>
        /// Refine the associated pose
        /// </summary>
        /// <param name="parameters">The parameters that we are calculating with</param>
        /// <param name="center">The central point</param>
        /// <param name="rotation">Defines the guess for the given rotation</param>
        /// <param name="points">The points that we have found</param>
        /// <returns>The matrix that we are working with</returns>
        public static Matrix<double> Refine(Parameters parameters, MCvPoint2D64f center, Matrix<double> rotation, FramePoints points) 
        {
            // Setup the minimizer
            var scorer = new Scorer(points.LeftPoints, points.RightPoints, rotation, center);
            var guess = GuessPose(parameters, rotation); var errors = new double[points.PointCount];

            // Minimize            
            var solver = new LMSolver(epsilon:1e-15);
            var result = solver.Minimize(scorer.CalculateCost, guess, points.PointCount);

            // Get the final pose
            var pose = scorer.BuildPose(result.OptimizedParameters);
           
            // Show the final score
            scorer.CalculateCost(result.OptimizedParameters, errors);           
            var total = errors.Sum(); var mean = total / points.PointCount;
            Console.WriteLine("Average Error: {0}", mean);

            // Return the final pose
            return pose;
        }

        #endregion

        #region Utility Functionality

        /// <summary>
        /// Defines the functionality to guess the associated translation
        /// </summary>
        /// <param name="parameters">The parameters associated with the application</param>
        /// <param name="rotation">The rotation associated with the application</param>
        /// <returns>The resultant ttranslation</returns>
        private static double[] GuessPose(Parameters parameters, Matrix<double> rotation)
        {
            // Guess the translation (Shift in x)
            var X = (parameters.GridX + 2) * parameters.BlockSize; var Y = 0; var Z = parameters.BlockSize;

            //retrieve the assocaiated rotation vector
            var rotVec = new Matrix<double>(3, 1); CvInvoke.Rodrigues(rotation, rotVec);

            var angle = rotVec.Data[1, 0];

            // Build and return the result
            return new double[] { 0, -angle, 0, X, Y, Z };
        }

        #endregion
    }
}
