﻿using Emgu.CV;
using Emgu.CV.Structure;
using Poser.Model;
using Poser.Tsai;
using System;
using System.Collections.Generic;

namespace Poser.Utility
{
    /// <summary>
    /// Defines the logic to acquire a given score of a pose guess
    /// </summary>
    public class Scorer
    {
        #region Private Variables

        private List<GridPoint> _grid1;
        private List<GridPoint> _grid2;
        private MCvPoint2D64f _center;
        private Matrix<double> _rotation;
 
        #endregion

        #region Main Constructors

        /// <summary>
        /// main Constructor
        /// </summary>
        /// <param name="grid1">The first grid that we are working with</param>
        /// <param name="grid2">The second grid that we are working with</param>
        /// <param name="rotation">The rotation associated</param>
        /// <param name="center">The chosen center piece</param>>
        public Scorer(List<GridPoint> grid1, List<GridPoint> grid2, Matrix<double> rotation, MCvPoint2D64f center) 
        {
            _center = new MCvPoint2D64f(center.X, center.Y);
            _grid1 = grid1; _grid2 = grid2; _rotation = rotation;
        }

        #endregion

        #region Score Calculation

        /// <summary>
        /// Calculate the associated cost of the given score
        /// </summary>
        /// <param name="parameters">The input parameters</param>
        /// <param name="errors">the associated errors</param>
        public void CalculateCost(double[] parameters, double[] errors) 
        {
            // Build the feature points
            var pose = BuildPose(parameters);
            var tgrid2 = TransformPoints(pose, _grid2); var points = _grid1.ConvertAll(i => i); points.AddRange(tgrid2);

            // Calculate the L matrix
            var solver = new LSolver(points, _center);
            var L = solver.CalculateLMatrix();

            // Perform calibration
            CalibrationResult calibration = GetCalibrationResult(_center, points, L, true);
            if (calibration.Focal < 0) calibration = GetCalibrationResult(_center, points, L, false);

            // Get the scores
            var score = ErrorFinder.GetError(points, calibration, errors);
           // Console.WriteLine("Score: " + score);
        }

        #endregion

        #region Find the calibration Result

        /// <summary>
        /// Get the calibration result from the data
        /// </summary>
        /// <param name="centre">The centre that we are dealing with</param>
        /// <param name="features">The associated features</param>
        /// <param name="L">The L matrix</param>
        /// <param name="positiveTy">Indicates if we should assume Ty to be positive result</param>
        /// <returns>The calibration result</returns>
        private static CalibrationResult GetCalibrationResult(MCvPoint2D64f centre, List<GridPoint> features, Matrix<double> L, bool positiveTy)
        {
            var calibration = LExtractor.Extract(L, positiveTy, centre);
            var finder = new FocalFinder(features, centre, calibration.Rotation, calibration.Translation[1, 0]);
            var fandTz = finder.CalculateResult();
            calibration.Focal = fandTz[0, 0]; calibration.Translation[2, 0] = fandTz[1, 0];
            return calibration;
        }

        #endregion

        #region Point Transformation helpers

        /// <summary>
        /// Build the pose from which we are working from
        /// </summary>
        /// <param name="parameters">The associated parameters that the system has</param>
        /// <returns>The pose that we are working from</returns>
        public Matrix<double> BuildPose(double[] parameters)
        {
            // Setup the associated variable
            var result = new Matrix<double>(4, 4); result.SetIdentity();

            // Do the rototation thing
            var rotation = BuildRotation(parameters);
            CopyRotation(result, rotation);

            // Do the translation part
            result.Data[0, 3] = parameters[3];
            result.Data[1, 3] = parameters[4];
            result.Data[2, 3] = parameters[5];
            
            // Return the result
            return result;
        }

        /// <summary>
        /// Build the rotation matrix that we are dealing with
        /// </summary>
        /// <param name="angle">The angle of rotation in the Z plane</param>
        /// <returns>The rotation matrix that have generated</returns>
        private Matrix<double> BuildRotation(double[] parameters)
        {
            var rotVec = new Matrix<double>(3, 1); CvInvoke.Rodrigues(_rotation, rotVec);
            var rotationVector = new Matrix<double>(new double[] { parameters[0], parameters[1], parameters[2]});
            var rotation = new Matrix<double>(3, 3); CvInvoke.Rodrigues(rotationVector, rotation);
            return rotation;
        }

        /// <summary>
        /// Copty the rotation to the result
        /// </summary>
        /// <param name="result">The result that we are copying to</param>
        /// <param name="rotation">The rotation that we have copied</param>
        private void CopyRotation(Matrix<double> result, Matrix<double> rotation)
        {
            for (int row = 0; row < 3; row++) 
            {
                for (int column = 0; column < 3; column++)
                {
                    result.Data[row, column] = rotation.Data[row, column];
                }
            }
        }

        /// <summary>
        /// Defines the logic to transform a set of points
        /// </summary>
        /// <param name="pose">The pose of the associated points</param>
        /// <param name="grid">The grid that we are working with</param>
        /// <returns>The points that we are working with</returns>
        public static List<GridPoint> TransformPoints(Matrix<double> pose, List<GridPoint> grid)
        {
            var result = new List<GridPoint>();

            foreach (var gridPoint in grid) 
            {
                var point = gridPoint.ScenePoint;
                var X = (pose.Data[0, 0] * point.X) + (pose.Data[0, 1] * point.Y) + (pose.Data[0, 2] * point.Z) + pose.Data[0, 3];
                var Y = (pose.Data[1, 0] * point.X) + (pose.Data[1, 1] * point.Y) + (pose.Data[1, 2] * point.Z) + pose.Data[1, 3];
                var Z = (pose.Data[2, 0] * point.X) + (pose.Data[2, 1] * point.Y) + (pose.Data[2, 2] * point.Z) + pose.Data[2, 3];

                var newPoint = new GridPoint(new MCvPoint3D64f(X, Y, Z), gridPoint.ImagePoint);
                result.Add(newPoint);
            }

            return result;
        }

        #endregion
    }
}