﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Poser.Utility
{
    /// <summary>
    /// Utility Functions for displaying a point set
    /// </summary>
    public class PointDisplayer
    {
        #region Main Display Function

        /// <summary>
        /// Show the associated point set
        /// </summary>
        /// <param name="caption">The caption of the image that we are displaying</param>
        /// <param name="templatePoints">The points that we are displaying</param>
        /// <param name="matchPoints">The match points that we are adding</param>
        public static void Show(string caption, List<PointF> templatePoints, List<PointF> matchPoints) 
        {
            var size = FindCloudSize(templatePoints);
            var image = new Image<Bgr, byte>(size); image.SetZero();

            var pointCenter = FindPointCenter(templatePoints);
            var translation = new PointF(pointCenter.X - size.Width / 2.0f, pointCenter.Y - size.Height / 2.0f);


            var blue = new MCvScalar(255, 0, 0);
            var green = new MCvScalar(0, 255, 0);
            var red = new MCvScalar(0, 0, 255);

            // Add in the template points
            for (var i = 0; i < templatePoints.Count; i++ )
            {
                var location = FindLocation(templatePoints[i], translation);
                var color = (i == 0 || i == templatePoints.Count - 1) ? blue : green;

                CvInvoke.Circle(image, location, 3, color, 3);
            }

            // add in the match points
            foreach (var point in matchPoints) 
            {
                var location = FindLocation(point, translation);
                CvInvoke.Circle(image, location, 3, red, 3);
            }

            // Display value
            CvInvoke.Imshow(caption, image);
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Find the ideal size of the image for displaying the points
        /// </summary>
        /// <param name="points">The points that we want to display</param>
        /// <returns>The ideal size</returns>
        private static Size FindCloudSize(List<PointF> points)
        {
            var minX = double.MaxValue; var maxX = double.MinValue;
            var minY = double.MaxValue; var maxY = double.MinValue;

            foreach (var point in points) 
            {
                minX = Math.Min(minX, point.X);
                maxX = Math.Max(maxX, point.X);
                minY = Math.Min(minY, point.Y);
                maxY = Math.Max(maxY, point.Y);
            }

            int width = (int)Math.Ceiling(maxX - minX) + 100;
            int height = (int)Math.Ceiling(maxY - minY) + 100;

            return new Size(width, height);
        }

        /// <summary>
        /// Find the central points of the collection
        /// </summary>
        /// <param name="points">The points that we are finding</param>
        /// <returns>The points that we are dealing with</returns>
        private static PointF FindPointCenter(List<PointF> points)
        {
            var totalX = 0.0f; var totalY = 0.0f;

            foreach (var point in points) 
            {
                totalX += point.X; totalY += point.Y;            
            }

            return new PointF(totalX / points.Count, totalY / points.Count);
        }

        /// <summary>
        /// Find the location of the point where to plot it
        /// </summary>
        /// <param name="point">The point that we are dealing with</param>
        /// <param name="translation">The translation that we are dealing with</param>
        /// <returns>The point that we are dealing with</returns>
        private static Point FindLocation(PointF point, PointF translation)
        {
            var x = (int)Math.Round(point.X - translation.X);
            var y = (int)Math.Round(point.Y - translation.Y);
            return new Point(x, y);
        }

        #endregion
    }
}
