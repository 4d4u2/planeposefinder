﻿using Poser.Model;
using System.Collections.Generic;
using System.IO;

namespace Poser.Utility
{
    /// <summary>
    /// Defines the logic to render the associated points
    /// </summary>
    public class PlyWriter
    {
        #region Public Entry Point

        /// <summary>
        /// Defines the logic to render the associated grid points
        /// </summary>
        /// <param name="path">The points that we are rendering</param>
        /// <param name="points">The resultant points</param>
        public static void Write(string path, List<GridPoint> points) 
        {
            using (var writer = new StreamWriter(path)) 
            {
                WriterHeader(writer, points);
                WritePoints(writer, points);
            }
        }

        #endregion

        #region Write the header for the application

        /// <summary>
        /// Write the header for the app
        /// </summary>
        /// <param name="writer">The associated writer</param>
        /// <param name="points">The vertices that we are writing to disk</param>
        private static void WriterHeader(StreamWriter writer, List<GridPoint> points)
        {
            writer.WriteLine("ply");
            writer.WriteLine("format ascii 1.0");
            writer.WriteLine("comment Generated by SAT");
            writer.WriteLine("element vertex " + points.Count);
            writer.WriteLine("property float x");
            writer.WriteLine("property float y");
            writer.WriteLine("property float z");
            writer.WriteLine("property uchar red");
            writer.WriteLine("property uchar green");
            writer.WriteLine("property uchar blue");
            writer.WriteLine("end_header");
        }

        #endregion

        #region Generate the Logic to write the points to disk

        /// <summary>
        /// Write the associated points to disk
        /// </summary>
        /// <param name="writer">The writer that we are using</param>
        /// <param name="points">The points that we are writing</param>
        private static void WritePoints(StreamWriter writer, List<GridPoint> points)
        {
            var total = points.Count / 2;

            int counter = 0;
            foreach (var point in points)
            {

                var line = string.Format("{0} {1} {2} 0 255 0", point.ScenePoint.X, point.ScenePoint.Y, point.ScenePoint.Z);
                if (counter == 0) line = string.Format("{0} {1} {2} 255 255 255", point.ScenePoint.X, point.ScenePoint.Y, point.ScenePoint.Z);
                else if (counter == total) line = string.Format("{0} {1} {2} 255 255 255", point.ScenePoint.X, point.ScenePoint.Y, point.ScenePoint.Z);
                else if (counter > total) line = string.Format("{0} {1} {2} 255 0 0", point.ScenePoint.X, point.ScenePoint.Y, point.ScenePoint.Z);

                counter++;

                writer.WriteLine(line);
            }
        }

        #endregion
    }
}
